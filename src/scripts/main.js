window.addEventListener('load', (event) => {
    var learnMore = document.querySelector('section.app-section.app-section--image-culture');

    var joinSection = document.createElement('section');
    joinSection.className = 'app-section';
    learnMore.parentNode.insertBefore(joinSection, learnMore.nextSibling);

    var sectionHeader = document.createElement('h2');
    sectionHeader.className = 'app-title';
    var headerContent = document.createTextNode('Join Our Program');

    sectionHeader.appendChild(headerContent);
    joinSection.appendChild(sectionHeader);

    var sectionSubtitle = document.createElement('h3');
    sectionSubtitle.className = 'app-subtitle';
    var subtitleContent1 = document.createTextNode('Sed do eiusmod tempor incididunt');
    var subtitleContent2 = document.createTextNode('ut labore et dolore magna aliqua.');

    sectionSubtitle.appendChild(subtitleContent1);
    sectionSubtitle.appendChild(document.createElement('br'));
    sectionSubtitle.appendChild(subtitleContent2);
    joinSection.appendChild(sectionSubtitle);

    var formContainer = document.createElement('form');
    formContainer.className = 'app-form';
    var emailForm = document.createElement('input');
    emailForm.setAttribute('type', 'email');
    emailForm.setAttribute('name', 'email');
    emailForm.setAttribute('placeholder', 'Email');

    joinSection.appendChild(formContainer);
    formContainer.appendChild(emailForm);

    var formButton = document.createElement('button');
    formButton.className = 'app-section__button app-section__button--subscribe';
    formButtonText = document.createTextNode('Subscribe');
    formButton.appendChild(formButtonText);

    formContainer.appendChild(formButton);

    function media(width) {
        if (width.matches) {
            joinSection.style.height = 525 + 'px';

            emailForm.style.width = 80 + 'vw';

            formContainer.style.display = 'flex';
            formContainer.style.justifyContent = 'center';
            formContainer.style.alignItems = 'center';
            formContainer.style.flexDirection = 'column';

            formButton.style.margin = 47 + 'px auto';

        } else {
            joinSection.style.height = 436 + 'px';
            joinSection.style.background = 'url(assets/images/join-bg-photo.png) no-repeat center/cover';
            joinSection.style.color = 'white';

            formContainer.style.marginTop = 41 + 'px';
            formContainer.style.flexDirection = 'row';

            emailForm.style.all = 'unset';
            emailForm.style.width = 'calc(1200px/3)';
            emailForm.style.padding = .5 + 'em ' + 1 + 'em';
            emailForm.style.backgroundColor = 'rgba(255, 255, 255, .15)';
            emailForm.style.fontWeight = 400;

            formButton.style.borderRadius = 26 + 'px';
            formButton.style.fontSize = 14 + 'px';
            formButton.style.height = 36 + 'px';
            formButton.style.width = 135 + 'px';
            formButton.style.letterSpacing = 1.2 + 'px';
            formButton.style.marginLeft = 30 + 'px';
            formButton.style.textTransform = 'uppercase';
        }
    }

    var width = window.matchMedia("(max-width: 768px)");
    media(width); // Call listener function at run time
    width.addEventListener('change', media); // Attach listener function on state changes

    formContainer.addEventListener('submit', function (e) {
        e.preventDefault();
        console.log(emailForm.value);
    }, false);
  });
